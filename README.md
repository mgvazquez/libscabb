*libscabb* (v1.3.8)
===================

*libscabb*, es una pequeña libreria de funciones escritas en bash que nos facilita diversas tareas a la hora de escribir nuestros scripts.
El archivo `example.tgz`, dentro de la carpeta example, contiene un template de script bash basado en *libscabb*, junto con su correspondiente archivo de configuracion el cual se carga automaticamente el invocar el script (el archivo .cfg, debe tener el mismo nombre que el archivo del script).
